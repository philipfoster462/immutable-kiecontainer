# Immutable KIE Server Openshift Deployment
This project will deploy a KJAR into an immutable KIE server. Project also includes additional steps to install RHPAM Monitoring on Openshift.

## Prerequisites
- Openshift 3.11 Cluster
- Enterprise Git server (ex. Github)
- Existing RHPAM project
- External RDBMS (see list of [supported databases](https://access.redhat.com/articles/3405381))
- Java JDK 8+
- Jenkins (optional)

Ensure that your openshift cluster has the RHPAM 7.7 image streams in the `openshift` namespace. 
If not, `oc apply -f rhpam77-image-streams.yaml -n openshift` to add them. Your cluster will need to have a service account setup with the Red Hat registry.  If this is necessary, an alert with documentation information will pop up under the created image streams. 

## Steps to Run RHPAM Openshift Install from CLI
1. `oc login <openshift_cluster_endpoint>`
2. `oc project <openshift_namespace>`
3. Generate a SSL keystore. `keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -storepass password -validity 360 -keysize 2048`.
4. Export the keystore with `cat keystore.jks | base64`, and copy this value into the `keystore.jks` field in `keystore.yaml`
5. Create a secure username and password in base64 format, then copy them into the `KIE_ADMIN_PWD` and `KIE_ADMIN_USER` fields in `kie-credentials.yaml`
6. `oc apply -f keystore.yaml`
7. `oc apply -f kie-credentials.yaml`
8. Update the properties file relevant to your deployment environment under vars/dev.properties. Documentation for all available parameters is available with `oc process --parameters -f rhpam77-prod-immutable-kieserver.yaml`. Not all fields are required.
9. `oc process -f rhpam77-prod-immutable-kieserver.yaml --param-file=vars/dev.properties | oc create -f -`

## Steps to Run RHPAM Monitoring Install from CLI 
1. Go into the same namespace used to deploy the immutable KIE server `oc project <openshift_namespace>`.
2. Using the same `keystore.jks` used for the immutable KIE server secret, create a secret for monitoring `oc create secret generic monitoring-app-secret --from-file=keystore.jks`.
3. And for the smart router `oc create secret generic smartrouter-app-secret --from-file=keystore.jks`.
4. Update the properties file relevant to your deployment environment under vars/devMonitoring.properties. Documentation for all available parameters is available with `oc process --parameters -f rhpam77-prod-immutable-kieserver.yaml`. Not all fields are required.
5. `oc process -f rhpam77-prod-immutable-monitor.yaml --param-file=vars/devMonitoring.properties | oc create -f -`

## Jenkins Setup
TODO: Write this section
<!-- https://github.com/jenkinsci/openshift-client-plugin#configuring-an-openshift-cluster -->
<!-- https://github.com/openshift/jenkins-sync-plugin -->
